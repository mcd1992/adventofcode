#![allow(unused)]

extern crate csv;

use std::path::Path;
use std::fs::File;
use std::mem;
use std::env;
use std::fmt::Debug;
use std::io::prelude::*;

/*
For each row, determine the difference between the largest value and the smallest value.
The checksum is the sum of all of these differences.

!vec[usize, cols], rows
*/

/// Default methods that should be on any Cell-like struct
trait CellMethods : Debug { // CellMethods inherits Debug trait
    /// Set a cell's position to (row, column)
    fn set_pos(&mut self, row: usize, col: usize);
}

#[derive(Clone,Debug)]
struct Cell<T> {
    data: T,
    pos: Option<(usize, usize)>,
}
impl<T> Cell<T> {
    /// Create a cell with data set to value: T
    pub fn new(data: T) -> Cell<T>
    where Self: Sized { // Generic trait methods only compatible with sized types
        Cell {
            data,
            pos: None,
        }
    }
}
impl<T> CellMethods for Cell<T>
    where T: Sized + Debug {
    fn set_pos(&mut self, row: usize, col: usize) {
        self.pos = Option::Some((row, col));
    }
}

#[derive(Debug)]
struct Sheet {
    rows: Vec<Vec<Box<CellMethods>>>, // 2D Vec of T: CellMethods
}

impl Sheet {
    pub fn new() -> Sheet {
        Sheet {
            rows: Vec::with_capacity(0)
        }
    }
    pub fn set_cell<T: 'static + CellMethods>(&mut self, row: usize, col: usize, mut cell: T) {
        self._ensure_row(row);
        self._ensure_col(row, col);
        cell.set_pos(row, col);
        self.rows[row].insert(col, Box::new(cell) );
    }
    pub fn get_row(&mut self, row: usize) -> &Vec<Box<CellMethods>> {
        self._ensure_row(row);
        &self.rows[row]
    }

    /// Make sure row `target_row` exists in our sheet
    fn _ensure_row(&mut self, target_row: usize) {
        let num_rows = self.rows.len();
        if num_rows < target_row+1 {
            for i in 0..=(target_row-num_rows) {
                self.rows.push( Vec::with_capacity(0) );
            }
        }
        //println!("rows was {}, now {}", num_rows, self.rows.len());
    }

    /// Make sure row `target_col` exists in `target_row`
    fn _ensure_col(&mut self, target_row: usize, target_col: usize) {
        self._ensure_row(target_row);
        let num_cols = self.rows[target_row].len();
        if num_cols < target_col+1 {
            for i in num_cols..target_col {
                self.rows[target_row].insert( i, Box::new(Cell::new(None::<usize>)) );
            }
        }
        //println!("cols was {}, now {}", num_cols, self.rows[target_row].len());
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Missing argument: <file>")
    }

    // Read input file and parse with csv crate
    let path = Path::new(&args[1]);
    let mut file = File::open(&path).expect("failed to open file");
    let mut parse = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(false)
    .from_reader(file);

    // Create and populate our spreadsheet
    let mut sheet: Vec<Vec<usize>> = vec![];
    for (irow, res) in parse.records().enumerate() {
        let mut icol = 0;
        let row: csv::StringRecord = res.expect("unable to parse row");
        let mut vrow: Vec<usize> = vec![];
        for (icol, cell) in row.iter().enumerate() {
            let val = cell.parse::<usize>().unwrap();
            vrow.push(val);
        }
        sheet.push(vrow);
    }

/* Part 1
    let mut diffs: Vec<usize> = vec![];
    for mut row in sheet.iter_mut() {
        row.sort();
        let diff = row[row.len()-1] - row[0];
        diffs.push(diff);
    }
    let cksum: usize = diffs.iter().sum();
    println!("{:#?}", cksum);
*/

    let mut results: Vec<usize> = vec![];
    for (irow, mut row) in sheet.iter_mut().enumerate() {
        let mut pushed = false;
        for i in 0..row.len()-1 { // Current left-hand check index
            for c in 0..row.len()-1 { // Current right-hand check index
                if (i != c) {
                    let ival = row[i];
                    let cval = row[c];
                    if (ival > 0 && cval > 0 && (ival % cval == 0)) {
                        println!("[{}, {}] {} % {} == 0", irow, c, ival, cval);
                        pushed = true;
                        results.push(ival / cval);
                    }
                }
            }
        }
        if (!pushed) {
            println!("no divs found on row {}", irow);
            results.push(0);
        }
    }

    let sum: usize = results.iter().sum();
    println!("{:?} \n {:#?}", results, sum);
}
