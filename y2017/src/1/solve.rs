use std::path::Path;
use std::fs::File;
use std::env;
use std::io::prelude::*;

/*
Find the sum of all digits that match the next digit in the list.
The list is circular, so the digit after the last digit is the first digit in the list.

https://doc.rust-lang.org/std/collections/index.html
https://github.com/udoprog/rust-advent-of-code-2017/blob/master/src/day01.rs
*/

fn solve<F>(digits: &[u32], offset: F) -> usize
    where F: Fn(usize) -> usize { // Closures as params...
    let mut sum: usize = 0;
    let length = digits.len();
    for index in 0..length {
        let i = index % length; // Rollover to 0 at end of Vec
        let n = (i + offset(length)) % length;
        if digits[i] == digits[n] {
            println!("digits[{}] = {} == digits[{}] = {}", i, digits[i], n, digits[n]);
            sum += digits[i] as usize;
        } else {
            println!("digits[{}] = {} != digits[{}] = {}", i, digits[i], n, digits[n]);
        }
    }
    sum
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Missing argument: <file>")
    }

    // Read input file and populate input vec
    let path = Path::new(&args[1]);
    let mut file = match File::open(&path) { // Could use the try! / ? macro here
        Err(why) => panic!("cant open file: {} [{}]", path.display(), why),
        Ok(file) => file,
    };
    let mut data: Vec<u8> = Vec::new();
    file.read_to_end(&mut data).expect("file.read_to_end failed:");
    let digits: Vec<u32> = data.iter().filter_map(|&c| {
        let c = c as char;
        c.to_digit(10)
    }).collect();

    let off1 = |_: usize| 1;
    let sol1 = solve(&digits, off1);

    let off2 = |len: usize| len / 2;
    let sol2 = solve(&digits, off2);

    println!("SOLUTION 1: {}", sol1);
    println!("SOLUTION 2: {}", sol2);

    // Iterate vec and check every index until end
    // If index -1 == index 0 then loop from 0 until we stop matching
}
